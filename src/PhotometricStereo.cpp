#include "PhotometricStereo.h"

PhotometricStereo::PhotometricStereo(std::string CALIBRATION, std::string MODEL, int NUM_IMGS, 
	bool RGB_MODE, bool GEN_MASK):
	CALIBRATION(CALIBRATION), MODEL(MODEL), RGB_MODE(RGB_MODE),
	NUM_IMGS(RGB_MODE ? 3 * NUM_IMGS : NUM_IMGS)
{
	ballMask = cv::imread(CALIBRATION + "mask.png", CV_LOAD_IMAGE_GRAYSCALE);
	imageLoadCheck(ballMask, CALIBRATION + "mask.png");
	calibImages.reserve(this->NUM_IMGS);
	modelImages.reserve(this->NUM_IMGS);
	lights = cv::Mat(this->NUM_IMGS, 3, CV_32F);

	loadImagesAndCalcLights();

	if (!GEN_MASK)
	{
		modelMask = cv::imread(MODEL + "mask.png", CV_LOAD_IMAGE_GRAYSCALE);
		imageLoadCheck(modelMask, MODEL + "mask.png");
		cv::threshold(modelMask, modelMask, 200, 255, CV_THRESH_BINARY);
	}
	else
		getModelMask();
}

void PhotometricStereo::imageLoadCheck(const cv::Mat &input, const std::string &filename)
{
	if (input.data == NULL)
		std::cout << "Can't load \"" << filename << "\" file.\n";
}

void PhotometricStereo::loadImagesAndCalcLights()
{
	if (RGB_MODE)
	{
		for (int j = 0; j < NUM_IMGS / 3; j++)
		{
			cv::Mat calib = cv::imread(CALIBRATION + std::to_string(j) + ".png", CV_LOAD_IMAGE_COLOR);
			imageLoadCheck(calib, CALIBRATION + std::to_string(j) + ".png");
			cv::Mat model = cv::imread(MODEL + std::to_string(j) + ".png", CV_LOAD_IMAGE_COLOR);
			imageLoadCheck(model, MODEL + std::to_string(j) + ".png");

			cv::Mat calibBGR[3], modelBGR[3];

			cv::split(calib, calibBGR);
			cv::split(model, modelBGR);
			std::cout << "Light directions\n";
			for (int i = 0; i < 3; i++)
			{
				cv::Vec3f light = getLightDirFromSphere(calibBGR[i]);
				std::cout << 3 * j + i << ": " << light[0] << " " << light[1] << " " << light[2] << std::endl;
				lights.at<float>(3 * j + i, 0) = light[0];
				lights.at<float>(3 * j + i, 1) = light[1];
				lights.at<float>(3 * j + i, 2) = light[2];

				calibImages.push_back(std::move(calibBGR[i]));
				modelImages.push_back(std::move(modelBGR[i]));
			}
		}
	}
	else
	{
		for (int j = 0; j < NUM_IMGS; j++)
		{
			cv::Mat calib = cv::imread(CALIBRATION + std::to_string(j) + ".png",
				CV_LOAD_IMAGE_GRAYSCALE);
			imageLoadCheck(calib, CALIBRATION + std::to_string(j) + ".png");
			cv::Mat model = cv::imread(MODEL + std::to_string(j) + ".png",
				CV_LOAD_IMAGE_GRAYSCALE);
			imageLoadCheck(model, MODEL + std::to_string(j) + ".png");

			std::cout << "Light directions\n";
			cv::Vec3f light = getLightDirFromSphere(calib);
			std::cout << j << ": " << light[0] << " " << light[1] << " " << light[2] << std::endl;
			lights.at<float>(j, 0) = light[0];
			lights.at<float>(j, 1) = light[1];
			lights.at<float>(j, 2) = light[2];

			calibImages.push_back(std::move(calib));
			modelImages.push_back(std::move(model));
		}
	}
}

void PhotometricStereo::getModelMask()
{
	const int thresh = 25;
	cv::Mat threshed;
	cv::Mat merged(modelImages[0].size(), CV_8U, cv::Scalar::all(0));

	for (auto const &i : modelImages)
	{
		cv::Mat tmp;
		cv::GaussianBlur(i, tmp, cv::Size(5, 5), 0);
		tmp /= modelImages.size();
		merged += tmp;
	}
	
	cv::threshold(merged, threshed, thresh, 255, CV_THRESH_BINARY);

	std::vector<std::vector<cv::Point>> contours;
	findContours(threshed.clone(), contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
	assert(contours.size() > 0);

	auto it = std::max_element(contours.begin(), contours.end(),
		[](const std::vector<cv::Point> &a, const std::vector<cv::Point> &b) {
			return a.size() < b.size();
		});

	std::iter_swap(it, contours.begin());
	contours.resize(1);

	modelMask = cv::Mat(threshed.rows, threshed.cols, CV_8UC1, cv::Scalar(0, 0, 0));
	cv::drawContours(modelMask, contours, -1, cv::Scalar(255, 255, 255), CV_FILLED);
}

cv::Rect PhotometricStereo::getBallBoundingBox()
{
	std::vector<std::vector<cv::Point> > contours;

	cv::findContours(ballMask.clone(), contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
	assert(contours.size() > 0);
	
	auto it = std::max_element(contours.begin(), contours.end(),
		[](const std::vector<cv::Point> &a, const std::vector<cv::Point> &b) {
		return a.size() < b.size();
	});

	return cv::boundingRect(*it);
}

cv::Vec3f PhotometricStereo::getLightDirFromSphere(const cv::Mat &image) 
{
	const int THRESH = 140; 
	auto boundingbox = getBallBoundingBox();
	const float radius = boundingbox.width / 2.0f;

	cv::Mat binary;
	cv::threshold(image, binary, THRESH, 255, CV_THRESH_BINARY);
	cv::Mat subImage(binary, boundingbox);

	/* calculate center of pixels */
	cv::Moments m = cv::moments(subImage, true);
	cv::Point2f center(m.m10 / m.m00, m.m01 / m.m00);
	/* x,y are swapped here */
	float x = (center.y - radius) / radius;
	float y = (center.x - radius) / radius;
	float z = sqrt(1.0 - pow(x, 2.0) - pow(y, 2.0));

	return cv::Vec3f(x, y, z);
}

void PhotometricStereo::calcNormalMapAndGradients(float blue_coef, float red_coef)
{
	const int height = modelImages[0].rows;
	const int width = modelImages[0].cols;

	cv::Mat normals(height, width, CV_32FC3, cv::Scalar::all(0));
	pGrads = cv::Mat(height, width, CV_32F, cv::Scalar::all(0));
	qGrads = cv::Mat(height, width, CV_32F, cv::Scalar::all(0));
	cv::Mat lightsInv;
	
	cv::invert(lights, lightsInv, cv::DECOMP_SVD);

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			if (modelMask.at<uchar>(cv::Point(x, y)) < 120)
			{
				cv::Vec3f nullvec(0.0f, 0.0f, 1.0f);
				normals.at<cv::Vec3f>(cv::Point(x, y)) = nullvec;
				pGrads.at<float>(cv::Point(x, y)) = 0.0f;
				qGrads.at<float>(cv::Point(x, y)) = 0.0f;
				continue;
			}
			cv::Mat intensity(NUM_IMGS, 1, CV_32F);

			for (int i = 0; i < NUM_IMGS; i++)
			{
				if (i % 3 == 0) //BLUE
					intensity.at<float>(i, 0) = blue_coef * modelImages[i].at<uchar>(cv::Point(x, y));
				if (i % 3 == 1) //GREEN
					intensity.at<float>(i, 0) = modelImages[i].at<uchar>(cv::Point(x, y));
				if (i % 3 == 2) //RED
					intensity.at<float>(i, 0) = red_coef * modelImages[i].at<uchar>(cv::Point(x, y));
			}

			cv::Mat normalVec = lightsInv * intensity;
			float albedo = sqrt(normalVec.dot(normalVec));

			if (albedo > 0) { normalVec /= albedo; }
			if (normalVec.at<float>(2, 0) == 0) { normalVec.at<float>(2, 0) = 1.0; }

			normals.at<cv::Vec3f>(cv::Point(x, y)) = normalVec;
			pGrads.at<float>(cv::Point(x, y)) = normalVec.at<float>(0, 0) / normalVec.at<float>(2, 0);
			qGrads.at<float>(cv::Point(x, y)) = normalVec.at<float>(1, 0) / normalVec.at<float>(2, 0);
		}
	}
	cv::cvtColor(normals, normalMap, CV_BGR2RGB);
	normalMap.convertTo(normalMap, CV_8UC3, 255.0);
}

/* lambda - odchylenie aproksymowanych powierzchni
mi - odchylenie aproksymowanych krzywizn */
void PhotometricStereo::calcHeightMap(const float lambda, const float mi) 
{

	cv::Mat P(pGrads.rows, pGrads.cols, CV_32FC2, cv::Scalar::all(0));
	cv::Mat Q(pGrads.rows, pGrads.cols, CV_32FC2, cv::Scalar::all(0));
	heightMap = cv::Mat(pGrads.rows, pGrads.cols, CV_32FC2, cv::Scalar::all(0));

	assert(P.cols % 2 == 0);
	assert(P.rows % 2 == 0);

	cv::dft(pGrads, P, cv::DFT_COMPLEX_OUTPUT);
	cv::dft(qGrads, Q, cv::DFT_COMPLEX_OUTPUT);

	for (int i = 0; i < pGrads.rows; i++) {
		for (int j = 0; j < pGrads.cols; j++) {
			if (i != 0 || j != 0)
			{
				float u = 2 * CV_PI*i / pGrads.rows;
				float v = 2 * CV_PI*j / pGrads.cols;

				if (i > (pGrads.rows / 2 - 1))
					u -= 2 * CV_PI;
				if (j > (pGrads.cols / 2 - 1))
					v -= 2 * CV_PI;

				float uv = pow(u, 2) + pow(v, 2);
				float d = (1.0f + lambda)*uv + mi*pow(uv, 2);

				heightMap.at<cv::Vec2f>(i, j)[0] = (u*P.at<cv::Vec2f>(i, j)[1] + v*Q.at<cv::Vec2f>(i, j)[1]) / d;
				heightMap.at<cv::Vec2f>(i, j)[1] = (-u*P.at<cv::Vec2f>(i, j)[0] - v*Q.at<cv::Vec2f>(i, j)[0]) / d;
			}
		}
	}

	/* setting unknown average height to zero */
	heightMap.at<cv::Vec2f>(0, 0)[0] = 0.0f;
	heightMap.at<cv::Vec2f>(0, 0)[1] = 0.0f;

	cv::dft(heightMap, heightMap, cv::DFT_INVERSE | cv::DFT_SCALE | cv::DFT_REAL_OUTPUT);
}

void PhotometricStereo::calcCoeffs(float &blue_coeff, float &red_coeff,
	float leftBound, float rightBound, float step)
{
	double sumMin = std::numeric_limits<double>::max();
	std::cout << "Calculating coefficients\n";
	for (float red = leftBound; red < rightBound; red += step)
	{
		for (float blue = leftBound; blue < rightBound; blue += step)
		{
			calcNormalMapAndGradients(blue, red);
			calcHeightMap();
			double sum = calcBackgroundMSE();
			if (sum < sumMin)
			{
				red_coeff = red;
				blue_coeff = blue;
				sumMin = sum;
			}
			std::cout << "RED: " << red << " BLUE: " << blue << " s: " << std::setprecision(5) << (sum) << std::endl;
		}
	}
}

float PhotometricStereo::calcBackgroundMSE()
{
	float sum2 = 0;
	float bgMean = cv::mean(heightMap, 255 - modelMask).val[0];
	float bgPixels = modelMask.rows*modelMask.cols - cv::countNonZero(modelMask);

	for (int y = 0; y < heightMap.rows; y++)
	{ 
		auto modelMask_ptr = modelMask.ptr<uchar>(y);
		auto heightMap_ptr = heightMap.ptr<float>(y);
		for (int x = 0; x < heightMap.cols; x++)
		{
			if (*modelMask_ptr < 140) //background
			{
				sum2 += pow(*heightMap_ptr - bgMean, 2);
			}
			modelMask_ptr++;
			heightMap_ptr++;
		}
	}
	return sum2 / (bgPixels - 1);
}