#include "Visualise.h"

void Visualise::displayMesh(const PhotometricStereo &ps_object, bool use_mask)
{

	const int width = ps_object.heightMap.cols;
	const int height = ps_object.heightMap.rows;
		
	/* creating visualization pipeline which basically looks like this:
	vtkPoints -> vtkPolyData -> vtkPolyDataMapper -> vtkActor -> vtkRenderer */

	auto points = vtkSmartPointer<vtkPoints>::New();
	auto polyData = vtkSmartPointer<vtkPolyData>::New();
	auto modelMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	auto modelActor = vtkSmartPointer<vtkActor>::New();
	auto renderer = vtkSmartPointer<vtkRenderer>::New();
	auto vtkTriangles = vtkSmartPointer<vtkCellArray>::New();
	auto cleanPolyData = vtkSmartPointer<vtkCleanPolyData>::New();

	/* insert x,y,z coords */
	for (int y = 0; y<height; y++)
	{
		for (int x = 0; x<width; x++) 
		{	
			points->InsertNextPoint(x, y, ps_object.heightMap.at<float>(y, x));
		}
	}

	/* setup the connectivity between grid points */
	auto triangle = vtkSmartPointer<vtkTriangle>::New();
	triangle->GetPointIds()->SetNumberOfIds(3);

	for (int y = 0; y < height - 1; y++)
	{
		for (int x = 0; x < width - 1; x++)
		{
			if (!use_mask || ps_object.modelMask.at<uchar>(y, x) > 120)
			{
				triangle->GetPointIds()->SetId(0, x + (y * width));
				triangle->GetPointIds()->SetId(1, (y + 1) * width + x);
				triangle->GetPointIds()->SetId(2, x + (y * width) + 1);
				vtkTriangles->InsertNextCell(triangle);
			}
			if (!use_mask || ps_object.modelMask.at<uchar>(y + 1, x + 1) > 120)
			{
				triangle->GetPointIds()->SetId(0, (y + 1) * width + x);
				triangle->GetPointIds()->SetId(1, (y + 1) * width + x + 1);
				triangle->GetPointIds()->SetId(2, x + (y * width) + 1);
				vtkTriangles->InsertNextCell(triangle);
			}
		}
	}


	polyData->SetPoints(points);
	polyData->SetPolys(vtkTriangles);

	/* removing unconnected (outside the mask) points */
	if (use_mask)
	{
		auto polyDataCut = vtkSmartPointer<vtkPolyData>::New();
		cleanPolyData->SetOutput(polyDataCut);
		cleanPolyData->SetInputData(polyData);
		cleanPolyData->Update();
		polyData = polyDataCut;
	}

	/* create two lights */
	auto light1 = vtkSmartPointer<vtkLight>::New();
	light1->SetPosition(-1, 1, 1);
	renderer->AddLight(light1);
	auto light2 = vtkSmartPointer<vtkLight>::New();
	light2->SetPosition(1, -1, -1);
	renderer->AddLight(light2);

	/* meshlab-ish background */
	modelMapper->SetInputData(polyData);
	renderer->SetBackground(.45, .45, .9);
	renderer->SetBackground2(.0, .0, .0);
	renderer->GradientBackgroundOn();
	auto renderWindow = vtkSmartPointer<vtkRenderWindow>::New();

	renderWindow->SetSize(800, 800); //(width, height)
	renderWindow->AddRenderer(renderer);
	modelActor->SetMapper(modelMapper);

	/* setting some properties to make it look just right */
	modelActor->GetProperty()->SetSpecularColor(1, 1, 1);
	modelActor->GetProperty()->SetAmbient(0.2);
	modelActor->GetProperty()->SetDiffuse(0.2);
	modelActor->GetProperty()->SetInterpolationToPhong();
	modelActor->GetProperty()->SetSpecular(0.8);
	modelActor->GetProperty()->SetSpecularPower(8.0);

	modelActor->RotateX(180);

	/* rotating */
	/*
	modelActor->RotateX(-10);
	modelActor->RotateY(10);
	modelActor->RotateZ(-5);
	*/

	renderer->AddActor(modelActor);
	auto interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow);

	/* export mesh */
	auto plyExporter = vtkSmartPointer<vtkPLYWriter>::New();
	plyExporter->SetInputData(polyData);
	plyExporter->SetFileName("export.ply");
	plyExporter->SetColorModeToDefault();
	plyExporter->SetArrayName("Colors");
	plyExporter->Update();
	plyExporter->Write();

	/* save screenshot (prepare) */
	auto w2i = vtkSmartPointer<vtkWindowToImageFilter>::New();
	w2i->SetInput(renderWindow);
	auto pngWriter = vtkSmartPointer<vtkPNGWriter>::New();

	/* render mesh */
	renderWindow->Render();

	/*save png*/
	w2i->Update();
	pngWriter->SetInputConnection(w2i->GetOutputPort());
	pngWriter->SetFileName("render.png");
	pngWriter->Write();
	interactor->Start();
}
