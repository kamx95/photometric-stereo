#include "PhotometricStereo.h"
#include "Visualise.h"
#include <iostream>
#include <chrono>

int main()
{
    
	float blueCoeff = 1.1f;
	float redCoeff = 0.9f;

	PhotometricStereo ps("F:\\PWR\\Publikacja\\Ziarna\\kalib.rgb.", "F:\\PWR\\Publikacja\\Ziarna\\z_kolo.rgb.", 1, true, false);
		
	//ps.calcCoeffs(blueCoeff, redCoeff, 0.90, 1.10, 0.05);
	//std::cout << "Min MSE at RED: " << redCoeff << " BLUE: " << blueCoeff << "\n";
		
	ps.calcNormalMapAndGradients(blueCoeff, redCoeff);
	
	cv::imshow("Normalmap", ps.normalMap);
	cv::imwrite("normals.png", ps.normalMap);

	ps.calcHeightMap(0.5, 0.0);
	Visualise::displayMesh(ps);

    cv::waitKey();
    return 0;
}
