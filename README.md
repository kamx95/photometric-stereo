# Colour photometric stereo

## Purpose
It was an Engineer Diploma Thesis Project - "3D reconstruction of natural products using colour photometric stereo on the example of cereal grains".
The projects includes comparsion between colour and classic approach.

## Details
Depth map from gradient field was calculated using regularizated 2D Fourier Transform method (T. Wei i R. Klette, „Depth Recovery from Noisy Gradient Vector”).
The code is based on https://github.com/NewProggie/Photometric-Stereo, modifications:
- handling RGB input images
- calculating correction for colour inputs
- speeded up computation of normal map
- creating binary mask
- corrected gradient field integration method
- rewritten in object oriented C++

## Results

### Colour (left side) and classic (right side) approach comparsion:
<img src="/Results/examples_of_models.jpg" alt="3D models" height="500">

### Quantitative comparsion of obtained 3D models (using CloudCompare):
<img src="/Results/comparsion.jpg" alt="Comparsion" height="500">

