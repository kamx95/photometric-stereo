#pragma once
#include <opencv2\opencv.hpp>
#include <iostream>

class PhotometricStereo
{

private:
	const std::string CALIBRATION, MODEL;
	const bool RGB_MODE;
	const int NUM_IMGS;
	std::vector<cv::Mat> calibImages, modelImages;
	cv::Mat lights;
public:
	cv::Mat modelMask, ballMask;
	cv::Mat pGrads, qGrads;
	cv::Mat normalMap, heightMap;

public:
	PhotometricStereo(std::string CALIBRATION, std::string MODEL, int NUM_IMGS,
		bool RGB_MODE = true, bool GEN_MASK = true);
	void calcNormalMapAndGradients(float blue_coef = 1.f, float red_coef = 1.f);
	void calcHeightMap(const float lambda = 0.5f, const float mi = 0.f);
	void calcCoeffs(float &blue_coeff, float &red_coeff,
		float leftBound, float rightBound, float step);

private:
	void imageLoadCheck(const cv::Mat &input, const std::string &filename);
	void loadImagesAndCalcLights();
	void getModelMask();
	cv::Rect getBallBoundingBox();
	cv::Vec3f getLightDirFromSphere(const cv::Mat &image);
	float calcBackgroundMSE();
};